const express = require('express');
const {v4: uuidv4} = require('uuid');
const router = express.Router();

const ALLOWED_ORIGINS = 'http://localhost:3002';

const users = [{id: uuidv4(), name: 'Oli'}]

function isOriginAllowed(origin) {
  return ALLOWED_ORIGINS.includes(origin) ? origin : null
}


/* GET users listing. */
router.get('/', function (req, res, next) {
  // You can also use and configure the cors express middleware to do this at server level,
  // so you don't need to declare these headers for every endpoint.
  // https://expressjs.com/en/resources/middleware/cors.html

  res.set({
    'Access-Control-Allow-Credentials': true,
    'Access-Control-Allow-Origin': isOriginAllowed(req.headers.origin),
    'Access-Control-Allow-Headers': 'content-type, x-custom-header',
    'Access-Control-Allow-Methods': 'GET, POST',
  })

  res.send(users);
});


router.options('/', function (req, res, next) {
  res.set({
    'Access-Control-Allow-Origin': isOriginAllowed(req.headers.origin),
    'Access-Control-Allow-Credentials': true,
    'Access-Control-Allow-Headers': 'content-type, x-custom-header',
    'Access-Control-Allow-Methods': 'GET, POST',
  })

  res.send();
});

/* POST users listing. */
router.post('/', function (req, res, next) {
  res.set({
    'Access-Control-Allow-Origin': isOriginAllowed(req.headers.origin),
    'Access-Control-Allow-Credentials': true,
    'Access-Control-Allow-Headers': 'content-type, x-custom-header',
    'Access-Control-Allow-Methods': 'GET, POST',
  })

  users.push({id: uuidv4(), name: req.body.name})

  res.cookie('randomUuid', uuidv4(), {maxAge: 999999, domain: 'localhost', sameSite: 'lax'})
  res.status(201);
  res.send();
});

module.exports = router;
